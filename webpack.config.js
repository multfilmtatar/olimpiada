const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");

var config = {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                url: false,
                                sourceMap: true
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: [
                                    autoprefixer({
                                        overrideBrowserslist: ["ie >= 8", "last 4 version"]
                                    }),
                                    cssnano({
                                        preset: ["default", { discardComments: { removeAll: true } }]
                                    })
                                ],
                                sourceMap: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "[name].css" // а тут надо прописать имя css которое вы хотите
        })
    ]
};

var site = Object.assign({}, config, {
    name: "postcard",
    mode: "development",
    devtool: "source-map",
    entry: {
        site: "./web/css/site.scss"
    },
    output: {
        path: __dirname + "/web/css/"
    }
});

module.exports = [site];

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <div class="pre-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        <ul class="pre-header-menu clearfix">
                            <li class="pre-header-menu__item">
                                <a href="/img/olimpiada2019.pdf" target="_blank"
                                    class="pre-header-menu__link">Положение</a>
                            </li>
                            <li class="pre-header-menu__item">
                                <a href="/img/instruction.pdf" target="_blank"
                                    class="pre-header-menu__link">Инструкция</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <div class="callback-info text-right">
                            Обратная связь <span class="callback-info__mail">sakla-tests@mail.ru</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="logo">
                            <a href="http://sakla.ru/">
                                <img src="/img/logo.png" alt="" class="img-responsive">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-9">
                        <div class="page-title">
                            <img src="/img/page-title.png" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="section">
                <?= $content ?>
            </div>
        </div>
    </div>

    <?php $this->endBody() ?>
    <!— Yandex.Metrika counter —>
        <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode
                .insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(26461770, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
        </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/26461770" style="position:absolute; left:-9999px;" alt="" /></div>
        </noscript>
        <!— /Yandex.Metrika counter —>
</body>

</html>
<?php $this->endPage() ?>
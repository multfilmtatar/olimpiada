<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\RegistrationForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'id'=>'register-form',
    'options' => [
        'class' => 'form'
    ],
    'action' => '/register/register',
    'enableAjaxValidation'=>true,
    'validateOnBlur'=>false
]); ?>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($model, 'username')->textInput(['maxlength' => 50, 'autocomplete'=>'off', 'autofocus'=>true])->label('Email') ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-6">
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>
    </div>
</div>

<div class="form-group">
    <div class="text-center">
        <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary form-submit']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    $('form#register-form').on('beforeSubmit', function(){
        var yiiform = $(this);
        $.ajax({
            url: yiiform.attr('action'),
            type: 'POST',
            data: yiiform.serializeArray()
        })
        .done(function(data) {
           if(typeof data.error !== "undefined") {             
                $('#registrationform-username').parent().removeClass('has-success').addClass('has-error').find('.help-block-error').html(data.error);
            }
        });
        return false;
    });
JS;

$this->registerJs($js, \yii\web\View::POS_READY);
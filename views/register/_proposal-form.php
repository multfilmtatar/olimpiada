<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ProposalForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'options' => [
            'class' => 'form'
        ],
        'enableAjaxValidation' => true
    ]); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'document_number')->textInput(['autofocus' => true, 'placeholder' => 'Введите 6 цифр'])->label('Номер свидетельства о рождении') ?>
            <div class="form-control-info">
                III-КБ № <span class="red">723271</span>
            </div>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'birthday')->widget(DatePicker::class, [
                    'options' => ['placeholder' => 'Дата рождения'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            <div class="form-control-info">
                например, 2006-04-08 (год-месяц-число)
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="text-center">
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary form-submit', 'name' => 'proposal-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <p style="color: #0064aa; margin-top: 35px;">*Если не получается авторизоваться, то ваш куратор должен исправить
        ваши данные в своем личном кабинете.</p>
</div>
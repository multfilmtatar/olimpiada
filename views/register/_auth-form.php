<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [
        'class' => 'form'
    ],
    'action' => '/register/login'
]); ?>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
</div>

<div class="form-group">
    <div class="text-center">
        <?= Html::submitButton('Авторизоваться', ['class' => 'btn btn-primary form-submit', 'name' => 'Авторизоваться']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    $('form#login-form').on('beforeSubmit', function(){
        var yiiform = $(this);
        $.ajax({
            url: yiiform.attr('action'),
            type: 'POST',
            data: yiiform.serializeArray()
        })
        .done(function(data) {
           if(typeof data.error !== "undefined") {             
                $('#loginform-password').parent().removeClass('has-success').addClass('has-error').find('.help-block-error').html(data.error);
            }
        });
        return false;
    });
JS;

$this->registerJs($js, \yii\web\View::POS_READY);
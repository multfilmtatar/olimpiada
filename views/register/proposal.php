<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $loginModel app\models\LoginForm */
/* @var $registerModel \webvimark\modules\UserManagement\models\forms\RegistrationForm */

use yii\helpers\Html;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="auth-info">
        <div class="auth-info__title">
            Логин и пароль от аккаунта “Сакла”.
        </div>
        <div class="auth-info__text">
            Также можно использовать логин и пароль от аккаунтов<br>
            “Культурный дневник школьника” или “Дневник добрых дел”.
        </div>
    </div>

    <?= $this->render('_auth-form', [
            'model' => $loginModel
        ]); ?>

    <div class="create-account text-center">
        <a href="http://xn--c1abcnbeooeh4l.xn--p1ai/registration" class="create-account__link" target="_blank">Создать
            аккаунт</a>
        <div class="create-account__text">
            После регистрации Вы сможете приступить к прохождению тестов.
        </div>
    </div>

</div>
<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="test">
    <div>
        <div>
            <form class="test__form test-form">
                <div class="test-form__header clearfix">
                    <div class="test-form__qnumb"><span class="test-form__qnumb-num">1</span>
                        <div class="clearfix"></div><span class="test-form__qnumb-title">Вопрос</span>
                    </div>
                    <div class="test-form__title">Test 1</div>
                </div>
                <div class="test-form__group"><label class="test-form__label"><input type="radio"
                            class="test-form__radio" value="1"><span class="test-form__item">1</span></label></div>
                <div class="test-form__group"><label class="test-form__label"><input type="radio"
                            class="test-form__radio" value="2"><span class="test-form__item">2</span></label></div>
                <div class="test-form__group"><label class="test-form__label"><input type="radio"
                            class="test-form__radio" value="3"><span class="test-form__item">3</span></label></div>
                <div class="test-form__group"><label class="test-form__label"><input type="radio"
                            class="test-form__radio" value="4"><span class="test-form__item">4</span></label></div>
            </form>
            <div class="text-center"><button class="btn btn-default default-button"
                    type="submit">Выбрать</button><button class="btn btn-default default-button default-button_invert"
                    type="submit">Пропустить</button></div>
            <div class="test__thumb-wrap"><span class="test__thumb active">1</span><span
                    class="test__thumb">2</span><span class="test__thumb">3</span><span
                    class="test__thumb">4</span><span class="test__thumb">5</span><span
                    class="test__thumb">6</span><span class="test__thumb">7</span><span
                    class="test__thumb">8</span><span class="test__thumb">9</span><span
                    class="test__thumb">10</span><span class="test__thumb">11</span><span
                    class="test__thumb">12</span><span class="test__thumb">13</span><span
                    class="test__thumb">14</span><span class="test__thumb">15</span><span
                    class="test__thumb">16</span><span class="test__thumb">17</span><span
                    class="test__thumb">18</span><span class="test__thumb">19</span></div>
        </div>
    </div>
</div>
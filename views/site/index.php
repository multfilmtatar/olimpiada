<?php

/* @var $this yii\web\View */

$this->title = 'Олимпиада для учащихся';
?>


<h1>Республиканская интернет-олимпиада для учащихся 5-6 классов на знание правил безопасного поведения на дорогах.</h1>

<h2>Результаты</h2>

<div class="result-table">
    <div class="result-table__header">
        <div class="result-table__item">Место</div>
        <div class="result-table__item">Участник</div>
        <div class="result-table__item">Район</div>
        <div class="result-table__item">Куратор</div>
        <div class="result-table__item">Баллы</div>
    </div>
    <div class="result-table__body">

        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    1
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Ерёменко Артём Степанович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Елабужский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Валиханов Ильнар Рафисович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,294
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    2
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Чернова Софья Александровна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Нижнекамский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Такачева Марина Николаевна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,229
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    3
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Мансуров Даниил Дилюсович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Елабужский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Валиханов Ильнар Рафисович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,223
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    4
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Муратова Айгиза Ленаровна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Елабужский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Валиханов Ильнар Рафисович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,197
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    5
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Сорокин Данил Вадимович
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Казань
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Максина Равия Абдулловна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,167
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    6
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Белова Алена Вадимовна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Бугульминский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Крайнова Татьяна Михайловна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,143
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    7
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Докукин Алексей Андреевич
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Нижнекамский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Такачева Марина Николаевна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,124
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    8
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Бекмухамедова Динара Ильнуровна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Нижнекамский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Давлетшина Наталья Викторовна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    30,119
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    9
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Гермагентова Инзиля Вадимовна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Мензелинский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Ахатова Лилия Фаритовна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    29,28
                </div>
            </div>
        </div>
        <div class="result-table__row">
            <div class="result-table__item">
                <div class="result-table__num">
                    10
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__user">
                    Диденко Данил
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__district">
                    Нижнекамский район
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__curator">
                    Баранова Елена Михайловна
                </div>
            </div>
            <div class="result-table__item">
                <div class="result-table__sum">
                    29,277
                </div>
            </div>
        </div>


    </div>
    <div class="result-table__footer">
        Программа подвела итоги олимпиады, сложив баллы за время прохождения и количество правильных ответов. Программа
        автоматически исключает из рейтинга результаты прохождения олимпиады менее чем за 6 минут.
    </div>
</div>


<h2>Личный кабинет куратора</h2>

<div class="curator-enter">
    <span style="font-size: 24px;">1 </span> <a href="/user-management/auth/login"
        class="btn default-button">Авторизация</a>
    <br>
    <br>
    <span style="font-size: 24px;">2 </span> <a href="/curator" class="btn default-button">Вход (после авторизации)</a>
</div>



<!-- <h2>График проведения Олимпиады:</h2>

<div class="date-table">
    <div class="date-table__item">
        <div class="row">
            <div class="col-xs-6">
                <div class="date-table__date">
                    23.09.2019 - 30.09.2019
                </div>
            </div>
            <div class="col-xs-6">
                <div class="date-table__text">
                    Регистрация кураторов
                </div>
            </div>
        </div>
    </div>
    <div class="date-table__item">
        <div class="row">
            <div class="col-xs-6">
                <div class="date-table__date">
                    26.09.2019 - 04.10.2019
                </div>
            </div>
            <div class="col-xs-6">
                <div class="date-table__text">
                    Регистрация участников кураторами
                </div>
            </div>
        </div>
    </div>
    <div class="date-table__item">
        <div class="row">
            <div class="col-xs-6">
                <div class="date-table__date">
                    05.10.2019
                </div>
            </div>
            <div class="col-xs-6">
                <div class="date-table__text">
                    Олимпиада
                </div>
            </div>
        </div>
    </div>
    <div class="date-table__item">
        <div class="row">
            <div class="col-xs-6">
                <div class="date-table__date date-table__date_red">
                    10.10.2019
                </div>
            </div>
            <div class="col-xs-6">
                <div class="date-table__text">
                    Подведение итогов Олимпиады
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- 
<h2>Тестирование</h2>-->
<!-- 
<div class="test-info-2">
    <div class="test-info-2__text">
        Вам дается 30 минут на прохождение теста.<br>Нужно ответить на 30 вопросов.<br>1 попытка.
    </div>
    <div class="test-info-2__button-wrap">
        <!-- <div class="test-info-2__info">
            5 октября кнопка будет активна
        </div>
        <a href="javascript:;" class="btn default-button js-start-test">НАЧАТЬ ТЕСТИРОВАНИЕ</a>
        <!--[if lt IE 10]>   
        <style>
            .js-start-test {
                display: none;
            }
        </style>
        <a href="javascript:;" class="btn default-button disabled">НАЧАТЬ ТЕСТИРОВАНИЕ</a>
        <div class="alert alert-danger" role="alert" style="margin-top: 15px;">Данный браузер не поддерживается! Пожалуйста, воспользуйтесь одним из рекомендуемых ниже браузеров.</div> 
        <![endif]-->
<!--  </div>
</div>-->


<!-- <h2>Олимпиада завершена</h2>

<div class="olimp-result-info">
    <div class="olimp-result-info__title">
        Результаты Олимпиады будут доступны<br>
        <big>10 октября 2019 года</big><br>
        на сайте “Сакла”.
    </div>
    <div class="olimp-result-info__img">
        <img src="/img/olimp-result-info__img.jpg" alt="" class="img-responsive">
    </div>
</div> -->

<h2>Награждение</h2>

<div class="simple-text">
    <p>По итогам Олимпиады активным участникам вручается электронное свидетельство об участии.
        Победителями и призерами республиканской Олимпиады становятся 10 участников, занявших первые десять мест по
        сумме баллов, (награждаются медалями и кубками (за 1, 2 и 3 место), 10 дипломами и 10 призами). Награждение
        победителей Олимпиады, занявших 1-10 места, состоится в Управлении ГИБДД МВД по Республике Татарстан.</p>
</div>

<h2>Организационный комитет</h2>

<div class="simple-text">
    <div class="simple-text__item">
        <div>Председатель:</div>
        <div><big>Гречанникова Н.В.</big></div>
        <div>заместитель министра образования и науки Республики Татарстан.</div>
    </div>
    <div class="simple-text__item">
        <div>Заместитель председателя:</div>
        <div><big>Бикмухаметов Д.Р.</big></div>
        <div>заместитель начальника Управления ГИБДД МВД по Республике Татарстан.</div>
    </div>
    <div class="simple-text__item">
        <div>Члены оргкомитета:</div>
        <div><big>Шакиров Р.И.</big></div>
        <div>начальник отдела пропаганды БДД Управления ГИБДД МВД по Республике Татарстан;</div>
    </div>
    <div class="simple-text__item">
        <div><big>Бикчантаева С.А.</big></div>
        <div>начальник отдела пропаганды БДД ГБУ «Безопасность дорожного движения»;</div>
    </div>
    <div class="simple-text__item">
        <div><big>Попов В.Н.</big></div>
        <div>начальник отдела безопасности дорожного движения ГБУ «Научный центр безопасности жизнедеятельности»;</div>
    </div>
    <div class="simple-text__item">
        <div><big>Соркина Ж.В.</big></div>
        <div>начальник отдела дополнительного образования детей Министерства образования и науки Республики Татарстан.
        </div>
    </div>
    <div class="simple-text__item">
        <div>Секретарь оргкомитета:</div>
        <div><big>Рахматуллин И.И.</big></div>
        <div>старший инспектор отдела пропаганды БДД Управления ГИБДД МВД по Республике Татарстан.</div>
    </div>
</div>


<?php
    // $this->registerJsFile('/js/platform.js',
    //     ['depends' => [\yii\web\JqueryAsset::className()]]);
    // $this->registerJsFile('/js/index.js',
    //     ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
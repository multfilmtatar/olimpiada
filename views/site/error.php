<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1>Ошибка</h1>

    <div class="alert alert-danger text-center">
        Что-то пошло не так.
        <div class="text-center" style="margin-top: 15px;">
            <a href="/user-management/auth/logout" class="btn default-button">ОБНОВИТЬ</a>
        </div>
    </div>

</div>
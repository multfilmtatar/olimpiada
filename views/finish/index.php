<?php

/* @var $model \app\models\AR\Testing */

?>
<?php if ($model): ?>

<div class="test-result">
    <div class="test-result__title">
        Результат прохождения олимпиады
    </div>
    <div class="test-result__info">
        <div class="test-result__label">
            Правильно
        </div>
        <div class="test-result__text">
            <?= $model->sum ?> из 30
        </div>
    </div>
    <div class="test-result__info">
        <div class="test-result__label">
            Время
        </div>
        <div class="test-result__text">
            <?= $tm ?>
        </div>
    </div>
</div>
<div class="text-center">
    <a href="/user-management/auth/logout" class="btn default-button">ЗАВЕРШИТЬ</a>
</div>
<?php else: ?>
---
<?php endif; ?>
<div id="root"></div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.5.7/es5-shim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.5.7/es5-sham.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/json3/3.3.2/json3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.2/es6-shim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.2/es6-sham.min.js"></script>
<script src="https://wzrd.in/standalone/es7-shim@latest"></script>
<!-- <script src="other-libs.js"></script> -->

<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
<script src="https://unpkg.com/react@16/umd/react.production.min.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js" crossorigin></script>

<script src="/js/test/index.js" type="text/babel"></script>


<?php
$script = <<< JS

    $.ajax({
        'data' : {'field': ''},
        'dataType' : 'json',
        'success' : function(data) {
            console.log(data);
        },
        'type' : 'post',
        'url' : '/api/index'
    })
    .fail(function(data) {
        console.log(data);
    });

JS;

$script2 = <<< JS
    $.ajax({
        'data' : {'question_id': 7, 'answer_id': 5},
        'dataType' : 'json',
        'success' : function(data) {
            console.log(data);
        },
        'type' : 'post',
        'url' : '/api/answer'
    }).fail(function(data) {
        console.log(data);
    });
JS;


// $this->registerJsFile(
//     'https://unpkg.com/react@16/umd/react.development.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );
// $this->registerJsFile(
//     'https://unpkg.com/react-dom@16/umd/react-dom.development.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );
// $this->registerJsFile(
//     'https://unpkg.com/babel-standalone@6.26.0/babel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );
// $this->registerJsFile(
//     'js/test/index.js', ['depends' => [\yii\web\JqueryAsset::className()]]
// );

// $this->registerJs($script, yii\web\View::POS_READY);
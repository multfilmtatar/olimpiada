<?php

use yii\helpers\Url;

?>

<h1>Республиканская интернет-олимпиада для учащихся 5-6 классов на знание правил безопасного поведения на дорогах.</h1>

<div class="test-info">
    <div class="row">
        <div class="col-xs-5">
            <div class="test-info__text">
                После нажатия на кнопку "Начать тестирование" Вам дается 30 минут на прохождение теста. Нужно ответить
                на 30 вопросов.
            </div>
        </div>
        <div class="col-xs-7">
            <div class="row">
                <div class="col-xs-4">
                    <div class="test-info__item">
                        <div class="test-info__ico">
                            <img src="/img/test-info__ico-1.png" alt="" class="img-responsive">
                        </div>
                        <div class="test-info__num">
                            30
                        </div>
                        <div class="test-info__label">
                            минут
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="test-info__item">
                        <div class="test-info__ico">
                            <img src="/img/test-info__ico-2.png" alt="" class="img-responsive">
                        </div>
                        <div class="test-info__num">
                            30
                        </div>
                        <div class="test-info__label">
                            вопросов
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="test-info__item">
                        <div class="test-info__ico">
                            <img src="/img/test-info__ico-3.png" alt="" class="img-responsive">
                        </div>
                        <div class="test-info__num">
                            1
                        </div>
                        <div class="test-info__label">
                            попытка
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="test-attention">
    <div class="test-attention__label">
        Внимание!
    </div>
    <div class="test-attention__text">
        Пройти тест можно только 1 раз.
    </div>
</div>

<div class="test-start">
    <a class="btn default-button" href="<?= Url::to('/test/start')?>">СТАРТ</a>
</div>
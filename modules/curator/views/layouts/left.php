<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Заявки', 'options' => ['class' => 'header']],
                    ['label' => 'Добавить', 'icon' => 'plus', 'url' => ['/curator/proposal/create']],
                    ['label' => 'Список', 'icon' => 'list-ul', 'url' => ['/curator/proposal']]
                ],
            ]
        ) ?>

    </section>

</aside>

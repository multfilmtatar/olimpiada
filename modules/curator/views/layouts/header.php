<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<img src="/img/page-title-2.png" alt="Олимпиада" class="img-responsive" style="margin-top: 7px;" />', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <a href="/user-management/auth/logout" class="btn btn-info"
                style="margin-top: 7px; margin-right: 10px;">Выход</a>
        </div>
    </nav>
</header>
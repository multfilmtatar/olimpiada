<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\AR\ProposalAR;

/* @var $this yii\web\View */
/* @var $model app\models\AR\ProposalAR */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-ar-view box box-primary">
    <div class="box-header">
        <?php if ($model->status_id === ProposalAR::STATUS_ACTIVE): ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'status_id',
                'username',
                'birthday',
                'document_number',
                'school',
                'class',
            ],
        ]) ?>
    </div>
</div>
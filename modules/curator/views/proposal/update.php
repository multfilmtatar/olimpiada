<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AR\ProposalAR */

$this->title = 'Update Proposal Ar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proposal Ars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proposal-ar-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

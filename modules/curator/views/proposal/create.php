<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AR\ProposalAR */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-ar-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

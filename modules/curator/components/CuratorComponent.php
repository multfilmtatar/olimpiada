<?php

namespace app\modules\curator\components;

use app\models\AR\CuratorAR;
use app\models\CuratorModel;
use webvimark\modules\UserManagement\models\User;
use yii\base\Component;

/**
 * Class CuratorComponent
 * @package app\modules\curator\components
 */

class CuratorComponent extends Component
{
    public $curator = null;

    public function __construct(array $config = [])
    {
        $this->curator = CuratorModel::findByUserId();
        parent::__construct($config);
    }

    /**
     * @return CuratorAR
     */
    public function getCurator()
    {
        return $this->curator;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->curator->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->curator->user;
    }
}

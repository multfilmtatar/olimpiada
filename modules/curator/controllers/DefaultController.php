<?php

namespace app\modules\curator\controllers;

use yii\web\Controller;

/**
 * Default controller for the `curator` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}

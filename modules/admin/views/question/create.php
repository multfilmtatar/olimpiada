<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AR\Question */
/* @var $answers \app\models\AR\Answer[] */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-create">

    <?= $this->render('_form', [
        'model' => $model,
        'answers' => $answers,
    ]) ?>

</div>

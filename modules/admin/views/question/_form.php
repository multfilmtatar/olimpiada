<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Question */
/* @var $form yii\widgets\ActiveForm */
/* @var $answers \app\models\AR\Answer[] */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="question-form box box-primary">

    <div class="box-body table-responsive">

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'imageFile')->fileInput([
            'accept' => 'image/jpg',
            'name' => 'question'
        ]) ?>

    </div>

    <div class="box-body table-responsive">

        <?php foreach ($answers as $index => $answer): ?>
            <div class="col-md-4">
                <?= $form->field($answer, "[$index]title")->textInput(['maxlength' => true]); ?>

                <?= $form->field($answer, "[$index]imageFile")->fileInput([
                    'accept' => 'image/jpg',
                    'name' => 'answer_'.$index
                ]); ?>

                <?= $form->field($answer, "[$index]isRight")->dropDownList([
                     0 => 'Неправильный вариант',
                     1 => 'Правильный вариант'
                 ]); ?>
            </div>
        <?php endforeach; ?>


    </div>

    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>

</div>

<?php ActiveForm::end(); ?>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProviders [] */

$this->title = 'Победители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personality-index box box-primary">
    <div class="box-header with-border">

    </div>
    <div class="box-body table-responsive no-padding">

        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($dataProviders as $key => $dataProvider): ?>
                <li role="presentation" class="<?= $key == 0 ? 'active' : ''?>">
                    <a role="tab" data-toggle="tab" href="#panel_<?= $key ?>"><?= $dataProvider['title'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="tab-content">
            <?php foreach ($dataProviders as $key => $dataProvider): ?>
                <div role="tabpanel" id="panel_<?= $key ?>" class="tab-pane fade in <?= $key == 0 ? 'active' : ''?>">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider['dataProvider'],
                        'layout' => "{items}\n{summary}\n{pager}",
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn'],

                            'proposal.username',
                            'proposal.school',
                            'proposal.class',
                            'proposal.phone',
                            'proposal.birthday',
                            'sum',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '',
                            ],
                        ],
                        'toolbar' =>  [
                            '{export}'
                        ],
                        'pjax' => true,
                        'bordered' => true,
                        'striped' => false,
                        'condensed' => false,
                        'responsive' => true,
                        'hover' => true,
                        'floatHeader' => true,
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY
                        ],
                    ]); ?>
                </div>
            <?php endforeach; ?>
        </div>

    </div>
</div>

<?php
$this->registerCssFile('/css/scroll.css');
$this->registerJsFile('/css/scroll.js', ['depends' => [\yii\bootstrap\BootstrapPluginAsset::class]]);

$script = <<< JS
$('.nav-tabs').scrollingTabs();
JS;

$this->registerJs($script, yii\web\View::POS_READY);
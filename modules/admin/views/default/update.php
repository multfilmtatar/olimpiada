<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подвести итоги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personality-index box box-primary">
    <div class="box-header with-border">

    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                    'attribute' => 'result',
                    'content' => function($data) {
                        return !$data->result ? Html::a('Подвести итог', ['/admin/default/result', 'id' => $data->id], ['class' => 'btn btn-xs btn-flat btn-primary']) : count($data->result);
                    }
                ],


                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
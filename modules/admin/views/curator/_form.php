<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user \webvimark\modules\UserManagement\models\User */
/* @var $curator \app\models\AR\CuratorAR */
/* @var $districts [] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($curator, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($user, 'password')->textInput(['maxlength' => true])?>

        <?= $form->field($user, 'repeat_password')->textInput(['maxlength' => true]) ?>

        <?= $form->field($curator, 'district_id')->dropDownList($districts, ['prompt' => 'Выберите район']) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

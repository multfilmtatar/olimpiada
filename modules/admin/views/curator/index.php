<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список кураторов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personality-index box box-primary">
    <div class="box-header with-border">

    </div>
    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'username',
                'user.username',
                'district.title',
                [
                    'label' => 'Количество пользователей',
                    'content'=>function(\app\models\AR\CuratorAR $data){
                        return $data->registeredUsers;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $user \webvimark\modules\UserManagement\models\User */
/* @var $curator \app\models\AR\CuratorAR */
/* @var $districts [] */

$this->title = 'Добавление куратора';
$this->params['breadcrumbs'][] = ['label' => 'Кураторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-create">

    <?= $this->render('_form', [
        'user' => $user,
        'curator' => $curator,
        'districts' => $districts,
    ]) ?>

</div>
<aside class="main-sidebar">
    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Кураторы', 'options' => ['class' => 'header']],
                    ['label' => 'Добавить', 'icon' => 'file-code-o', 'url' => ['/admin/curator/create']],
                    ['label' => 'Список', 'icon' => 'dashboard', 'url' => ['/admin/curator']]
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Тесты', 'options' => ['class' => 'header']],
                    ['label' => 'Добавить', 'icon' => 'file-code-o', 'url' => ['/admin/question/create']],
                    ['label' => 'Список', 'icon' => 'dashboard', 'url' => ['/admin/question']]
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Итоги', 'options' => ['class' => 'header']],
                    ['label' => 'Подвести итоги', 'icon' => 'file-code-o', 'url' => ['/admin/default/update']],
                    ['label' => 'Победители', 'icon' => 'dashboard', 'url' => ['/admin/default/winners']]
                ],
            ]
        ) ?>
    </section>
</aside>

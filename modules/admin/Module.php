<?php

namespace app\modules\admin;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (\Yii::$app->user->isGuest) {
            \Yii::$app->getResponse()->redirect(Url::home(true).'login', 301)->send();
            \Yii::$app->end();
        } else {
            if (!Yii::$app->user->isSuperadmin) {
                throw new NotFoundHttpException('Запрещен доступ');
            }
        }
    }
}

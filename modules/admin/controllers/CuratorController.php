<?php

namespace app\modules\admin\controllers;

use app\models\AR\CuratorAR;
use app\models\AR\DistrictsAR;
use app\models\CuratorModel;
use webvimark\modules\UserManagement\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

/**
 * Curator controller for the `curator` module
 */
class CuratorController extends Controller
{

    public function actionIndex()
    {
        $query = CuratorAR::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new CuratorModel();

        $user = $model->makeUser();
        $curator = $model->makeCurator();

        $post = Yii::$app->request->post();

        if ($user->load($post) && $curator->load($post)) {

            if ($user->save()) {
                $curator->user_id = $user->id;
                $curator->save();

                User::assignRole($user->id, 'curator');

                Yii::$app->session->setFlash('success', 'User created');
                return $this->redirect('index');
            } else {
                Yii::$app->session->setFlash('danger', 'User not created');
            }
        }

        return $this->render('create', [
            'user' => $user,
            'curator' => $curator,
            'districts' => ArrayHelper::map(DistrictsAR::find()->all(), 'id', 'title')
        ]);
    }
}

<?php

namespace app\modules\admin\controllers;

use app\models\AR\CuratorAR;
use app\models\AR\DistrictsAR;
use app\models\AR\ProposalAR;
use app\models\AR\ResultAR;
use app\models\AR\Testing;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpdate()
    {
        $query = DistrictsAR::find()->with('result');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('update', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionResult($id)
    {
        $district = DistrictsAR::findOne($id);

        $subQuery = CuratorAR::find()->select('id')->where(['district_id' => $district->id]);

        $proposals = ProposalAR::find()
            ->innerJoinWith(['test' => function(ActiveQuery $query) {
                $query->orderBy(['sum' => SORT_DESC]);
            }])
            ->where(['IN', 'curator_id', $subQuery])
            ->limit(20)
            ->all();

        $rows = [];

        foreach ($proposals as $proposal) {
            $rows[] = [
                'proposal_id' => $proposal->id,
                'district_id' => $district->id,
                'sum' => $proposal->test->sum
            ];
        }

        \Yii::$app->db->createCommand()->batchInsert(ResultAR::tableName(), [
            'proposal_id',
            'district_id',
            'sum'
        ], $rows)->execute();

        return $this->redirect('/admin/default/update');
    }

    public function actionWinners()
    {
        $dataProviders = [];
        $districts = DistrictsAR::find()->all();

        foreach ($districts as $district) {
            $dataProviders[] = [
                'title' => $district->title,
                'dataProvider' => new ActiveDataProvider([
                    'query' => ResultAR::find()->where(['district_id' => $district->id])->orderBy(['sum' => SORT_DESC]),
                    'pagination' => [
                        'pageSize' => 40,
                    ],
                ])
            ];
        }

        return $this->render('winners', [
            'dataProviders' => $dataProviders
        ]);
    }
}

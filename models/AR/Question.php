<?php

namespace app\models\AR;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $title Вопрос
 * @property string $img Изображение
 *
 * @property Answer $answers
 */
class Question extends \yii\db\ActiveRecord
{

    /**
     * @var UploadedFile
     */

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'img'], 'required'],
            [['title', 'img'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Вопрос',
            'img' => 'Изображение',
            'imageFile' => 'Изображение',
        ];
    }

    public function upload()
    {
        $name = 'files/' . Yii::$app->security->generateRandomString(20) . '.' . $this->imageFile->extension;
        $this->imageFile->saveAs($name);
        return $name;
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::class, ['question_id' => 'id']);
    }
}

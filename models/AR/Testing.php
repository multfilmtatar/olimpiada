<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "testing".
 *
 * @property int $id
 * @property int $proposal_id Заявка
 * @property int $user_id Пользователь
 * @property string $data Данные теста
 * @property int $sum Результат
 * @property string $token Токен доступа
 * @property int $time Дата и время начала тестирования
 * @property int $endtime
 */
class Testing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'testing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_id', 'user_id', 'sum', 'time', 'endtime'], 'integer'],
            [['data'], 'string'],
            [['token'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Заявка',
            'user_id' => 'Пользователь',
            'data' => 'Данные теста',
            'sum' => 'Результат',
            'token' => 'Токен доступа',
            'time' => 'Дата и время начала тестирования',
            'endtime' => 'Дата и время окончания тестирования',
        ];
    }
}

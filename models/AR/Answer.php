<?php

namespace app\models\AR;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "answer".
 *
 * @property int $id
 * @property int $question_id Вопрос
 * @property string $title Ответ
 * @property string $img Изображение
 * @property int $isRight Правильный
 */
class Answer extends \yii\db\ActiveRecord
{

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question_id', 'isRight'], 'integer'],
            [['title'], 'required'],
            [['title', 'img'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Вопрос',
            'title' => 'Ответ',
            'img' => 'Изображение',
            'imageFile' => 'Изображение',
            'isRight' => 'Правильный',
        ];
    }

    public function upload()
    {
        $name = 'files/' . Yii::$app->security->generateRandomString(20) . '.' . $this->imageFile->extension;
        $this->imageFile->saveAs($name);
        return $name;
    }
}

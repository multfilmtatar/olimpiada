<?php

namespace app\models\AR;

use webvimark\modules\UserManagement\models\User;
use Yii;

/**
 * This is the model class for table "curator".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $username ФИО
 * @property int $district_id Район
 *
 * @property int $registeredUsers
 *
 * @property User $user
 * @property DistrictsAR $district
 */
class CuratorAR extends \yii\db\ActiveRecord
{

//    public $registeredUsers = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'curator';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'district_id'], 'integer'],
            [['username', 'district_id'], 'required'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'username' => 'ФИО',
            'district_id' => 'Район',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getDistrict()
    {
        return $this->hasOne(DistrictsAR::class, ['id' => 'district_id']);
    }

    public function getRegisteredUsers()
    {
        return 0;
    }
}

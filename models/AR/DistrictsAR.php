<?php

namespace app\models\AR;

use Yii;
use yii\web\Response;

/**
 * This is the model class for table "districts".
 *
 * @property int $id
 * @property int $title Название
 */
class DistrictsAR extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'districts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Район',
            'result' => 'Результаты',
        ];
    }

    public function getResult()
    {
        return $this->hasMany(ResultAR::class, ['district_id' => 'id']);
    }
}

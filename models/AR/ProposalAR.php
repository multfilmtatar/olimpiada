<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "proposal".
 *
 * @property int $id
 * @property int $status_id
 * @property int $curator_id Куратор
 * @property string $username Имя пользователя
 * @property string $birthday Дата рождения
 * @property string $document_number № свидетельства о рождении
 * @property string $school Образовательная организация
 * @property string $class Класс
 * @property string $address Домашний адрес
 * @property string $phone Номер телефона
 * @property int $user_id Пользователь
 *
 * @property Testing $test
 */
class ProposalAR extends \yii\db\ActiveRecord
{

    CONST STATUS_ACTIVE = 1;
    CONST STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_id', 'curator_id', 'user_id', 'document_number'], 'integer'],
            [['username', 'birthday', 'document_number', 'school', 'class'], 'required'],
            [['birthday'], 'safe'],
            [['username', 'school', 'class', 'address', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status ID',
            'curator_id' => 'Куратор',
            'username' => 'ФИО участника',
            'birthday' => 'Дата рождения',
            'document_number' => '№ свидетельства о рождении',
            'school' => 'Образовательная организация',
            'class' => 'Класс',
            'address' => 'Домашний адрес',
            'phone' => 'Номер телефона',
            'user_id' => 'Пользователь',
            'result' => 'Результат',
        ];
    }

    public function getTest()
    {
        return $this->hasOne(Testing::class, ['user_id' => 'user_id']);
    }

    public function getResult()
    {
        return $this->test ? $this->test->sum : null;
    }
}
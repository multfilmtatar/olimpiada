<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "result".
 *
 * @property int $id
 * @property int $proposal_id Заявка
 * @property int $district_id Район
 * @property int $sum Итог
 */
class ResultAR extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['proposal_id', 'district_id', 'sum'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proposal_id' => 'Заявка',
            'district_id' => 'Район',
            'sum' => 'Правильных ответов',
        ];
    }

    public function getProposal()
    {
        return $this->hasOne(ProposalAR::class, ['id' => 'proposal_id']);
    }
}

<?php

namespace app\models;

use app\models\AR\ProposalAR;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;
use yii\web\Cookie;

/**
 * ProposalForm is the model behind the contact form.
 */
class ProposalForm extends Model
{
    public $document_number;
    public $birthday;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['document_number', 'birthday'], 'required'],
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            ['document_number', 'string'],
            ['document_number', 'validateExistence'],
            ['birthday', 'validateCouple'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     *
     */
    public function validateExistence($attribute, $params)
    {
        if (!ProposalAR::findOne(["$attribute" => $this->$attribute])) {
            $this->addError($attribute, "№".$this->$attribute." свидетельства о рождении не зарегистрировано");
        }
    }

    public function validateCouple($attribute, $params)
    {
        if (!ProposalAR::findOne(["document_number" => $this->document_number, 'birthday' => $this->birthday])) {
            $this->addError($attribute, "Пара ".$this->document_number . " и " . $this->birthday ." не зарегистрирована");
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'document_number' => '№ свидетельства о рождении',
            'birthday' => 'Дата рождения',
        ];
    }

    public function setCookies()
    {
        $prop = ProposalAR::findOne(['birthday' => $this->birthday, 'document_number' => $this->document_number]);

        Yii::$app->session->set('proposalId', $prop->id);
    }
}

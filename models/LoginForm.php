<?php

namespace app\models;

use app\models\AR\ProposalAR;
use webvimark\modules\UserManagement\models\User;
use Yii;
use yii\base\Model;
use yii\web\Cookie;

/**
 * LoginForm
 */
class LoginForm extends Model
{
    public $login;
    public $password;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['login', 'email']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Email',
            'password' => 'Пароль',
        ];
    }

    public function autoRegister(LoginForm $model)
    {
        if (($user = User::findOne(['username' => $model->login])) == null) {
            $user = new User();
            $user->status = User::STATUS_ACTIVE;
            $user->username = $model->login;
            $user->email = $model->login;
            $user->password = $model->password;
            $user->save(false);
        }
        return $user;
    }

}

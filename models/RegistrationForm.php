<?php
namespace app\models;

use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\base\Model;
use Yii;
use yii\helpers\Html;

class RegistrationForm extends Model
{
	public $username;
	public $password;
	public $repeat_password;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			[['username', 'password', 'repeat_password'], 'required'],
			[['username', 'password', 'repeat_password'], 'trim'],
            ['username', 'email'],

			['username', 'unique',
				'targetClass'     => 'webvimark\modules\UserManagement\models\User',
				'targetAttribute' => 'username',
			],

			['username', 'purgeXSS'],

			['password', 'string', 'max' => 255],
			['password', 'match', 'pattern' => Yii::$app->getModule('user-management')->passwordRegexp],

			['repeat_password', 'compare', 'compareAttribute'=>'password']
		];

		return $rules;
	}

	public function purgeXSS($attribute)
	{
		$this->$attribute = Html::encode($this->$attribute);
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'username'        => Yii::$app->getModule('user-management')->useEmailAsLogin ? 'E-mail' : UserManagementModule::t('front', 'Login'),
			'password'        => UserManagementModule::t('front', 'Password'),
			'repeat_password' => UserManagementModule::t('front', 'Repeat password'),
			'captcha'         => UserManagementModule::t('front', 'Captcha'),
		];
	}


    /**
     * @param bool $performValidation
     * @return bool|User
     */
	public function registerUser($performValidation = true)
	{
		if ( $performValidation AND !$this->validate() )
		{
			return false;
		}

		$user = new User();
		$user->password = $this->password;

        $user->email = $this->username;
        $user->username = $this->username;

        if ($user->save()) {
            return $user;
        } else {
            $this->addError('username', UserManagementModule::t('front', 'Login has been taken'));
        }
	}
}

<?php

namespace app\models;

use app\models\AR\CuratorAR;
use webvimark\modules\UserManagement\models\User;

class CuratorModel
{
    public function makeUser()
    {
        return new User(['scenario'=>'newUser']);
    }

    public function makeCurator()
    {
        return new CuratorAR();
    }

    /**
     * Поиск куратора по id пользователя
     *
     * @return null|CuratorAR
     */
    public static function findByUserId()
    {
        return CuratorAR::findOne(['user_id' => \Yii::$app->user->id]);
    }
}

<?php

namespace app\commands\strategy;


class PostgreSqlType extends InputAbstractClass implements DatabaseTypeInterface
{

    protected $host = 'localhost';

    protected $port = 5432;

    protected $dbName;

    public function start($type)
    {
        echo 'Choose a host (localhost): ';

        if (($host = $this->cleaningInput(fgets(STDIN))) != null) {
            $this->host = $host;
        }

        echo 'Enter a port (5432): ';

        if (($port = $this->cleaningInput(fgets(STDIN))) != null) {
            $this->port = $port;
        }

        $this->dbName = $this->inputDbName();

        $this->db = [
            'class' => 'yii\\db\\Connection',
            'dsn' => $type . ':host='.$this->host.';port='.$this->port.';dbname='.$this->dbName,
            'username' => $this->inputUserName(),
            'password' => $this->inputPassword(),
            'charset' => $this->inputCharset(),
        ];

        return $this->db;
    }

    protected function inputDbName()
    {
        echo 'Enter database name: ';

        $dbName = $this->cleaningInput(fgets(STDIN));

        return (!$dbName) ? $this->inputDbName() : $dbName;
    }

}
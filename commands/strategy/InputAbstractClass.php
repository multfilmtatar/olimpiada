<?php

namespace app\commands\strategy;


abstract class InputAbstractClass
{
    protected $username = 'root';

    protected $password = '';

    protected $charset = 'utf8';

    protected $db = [];

    public function cleaningInput($input)
    {
        return strtolower(trim($input));
    }

    public function inputUserName()
    {
        echo 'Enter username (root): ';

        if (($username = $this->cleaningInput(fgets(STDIN))) != null) {
            $this->username = $username;
        }

        return $this->username;
    }

    public function inputPassword()
    {
        echo 'Enter password: ';

        if (($password = $this->cleaningInput(fgets(STDIN))) != null) {
            $this->password = $password;
        }

        return $this->password;
    }

    public function inputCharset()
    {
        echo 'Enter charset (utf8): ';

        if (($charset = $this->cleaningInput(fgets(STDIN))) != null) {
            $this->charset = $charset;
        }

        return $this->charset;
    }

}
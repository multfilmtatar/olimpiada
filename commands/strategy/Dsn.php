<?php

namespace app\commands\strategy;


class Dsn
{
    protected $dsn;

    protected $type;

    public function __construct(DatabaseTypeInterface $dsn, $type)
    {
        $this->dsn = $dsn;
        $this->type = $type;
    }

    public function start()
    {
        return $this->dsn->start($this->type);
    }
}
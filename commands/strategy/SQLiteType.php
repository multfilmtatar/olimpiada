<?php

namespace app\commands\strategy;


class SQLiteType extends InputAbstractClass implements DatabaseTypeInterface
{

    protected $path;

    public function start($type)
    {
        $this->path = $this->inputPath();

        $this->db = [
            'class' => 'yii\\db\\Connection',
            'dsn' => $type . ':' . $this->path,
            'username' => $this->inputUserName(),
            'password' => $this->inputPassword(),
            'charset' => $this->inputCharset(),
        ];

        return $this->db;
    }

    protected function inputPath()
    {
        echo 'Enter the file path: ';

        $path = $this->cleaningInput(fgets(STDIN));

        return (!$path) ? $this->inputPath() : $path;
    }

}
<?php

namespace app\commands\strategy;


interface DatabaseTypeInterface
{
    public function start($type);
}
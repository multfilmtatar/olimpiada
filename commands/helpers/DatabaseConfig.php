<?php

namespace app\commands\helpers;

use app\commands\strategy\CubridType;
use app\commands\strategy\Dsn;
use app\commands\strategy\MsDbLibType;
use app\commands\strategy\MsSqlSrvType;
use app\commands\strategy\MsSqlType;
use app\commands\strategy\MysqlType;
use app\commands\strategy\OracleType;
use app\commands\strategy\PostgreSqlType;
use app\commands\strategy\SQLiteType;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;

class DatabaseConfig
{

    protected static $dbType = 'mysql';
    
    public static function create(Controller $ths)
    {
        echo $ths->ansiFormat('Allowed database types: mysql, sqlite, pgsql (PostgreSQL), cubrid, sqlsrv(MS SQL via sqlsrv driver), dblib (MS SQL via dblib driver), mssql (MS SQL via mssql driver), oci (Oracle) *' . PHP_EOL . PHP_EOL, Console::FG_CYAN);

        echo 'Select database type? (mysql): ';

        $dbTypeInput = strtolower(trim(fgets(STDIN)));

        if ($dbTypeInput) {
            self::$dbType = $dbTypeInput;
        }

        switch (self::$dbType) {
            case 'mysql':
                $dsn = new Dsn(new MysqlType(), self::$dbType);
                break;
            case 'sqlite':
                $dsn = new Dsn(new SQLiteType(), self::$dbType);
                break;
            case 'pgsql':
                $dsn = new Dsn(new PostgreSqlType(), self::$dbType);
                break;
            case 'cubrid':
                $dsn = new Dsn(new CubridType(), self::$dbType);
                break;
            case 'sqlsrv':
                $dsn = new Dsn(new MsSqlSrvType(), self::$dbType);
                break;
            case 'dblib':
                $dsn = new Dsn(new MsDbLibType(), self::$dbType);
                break;
            case 'mssql':
                $dsn = new Dsn(new MsSqlType(), self::$dbType);
                break;
            case 'oci':
                $dsn = new Dsn(new OracleType(), self::$dbType);
                break;
            default:
                echo $ths->ansiFormat('Unknown database type', Console::FG_RED) . PHP_EOL;
                exit();
        }

        $connection = $dsn->start();

        self::createDbConfig($connection, $ths);
    }

    protected function createDbConfig($conf, Controller $ths)
    {
        $directory = \Yii::getAlias('@app') . '/config/local/';

        if (!file_exists($directory)) {
            mkdir($directory);
        }

        $filename = $directory . 'db-local.php';

        if (!file_exists($filename)) {
            file_put_contents($filename, "<?php\nreturn " . VarDumper::export($conf) . ";\n", LOCK_EX);
            echo $ths->ansiFormat("File \"" . $filename . "\" created" . PHP_EOL, Console::FG_GREEN) ;
        }
    }
}
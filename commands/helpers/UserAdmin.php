<?php

namespace app\commands\helpers;

use webvimark\modules\UserManagement\models\User;
use yii\console\Controller;
use yii\helpers\Console;

class UserAdmin
{

    protected static $username = 'admin';

    protected static $password = 'admin';

    public static function add(Controller $ths)
    {
        echo 'Enter admin login? (admin): ';

        $username = strtolower(trim(fgets(STDIN)));
        if ($username) {
            self::$username = $username;
        }

        echo 'Enter admin password? (admin): ';

        $password = strtolower(trim(fgets(STDIN)));
        if ($password) {
            self::$password = $password;
        }

        $user = new User();
        $user->superadmin = 1;
        $user->status = User::STATUS_ACTIVE;
        $user->username = self::$username;
        $user->password = self::$password;
        $user->save(false);

        echo $ths->ansiFormat("User \"" . $user->username . "\" created" . PHP_EOL, Console::FG_GREEN) ;
    }

}
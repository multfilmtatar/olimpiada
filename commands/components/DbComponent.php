<?php

namespace app\commands\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;


class DbComponent extends Component
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public static function get($db)
    {
        $locale = \Yii::getAlias('@app') . '/config/local/db-local.php';

        if (file_exists($locale)) {
            $db = ArrayHelper::merge($db, require $locale);
        }
        return $db;
    }

}
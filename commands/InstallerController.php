<?php

namespace app\commands;

use app\commands\helpers\DatabaseConfig;
use app\commands\helpers\UserAdmin;
use yii\console\Controller;
use Yii;


class InstallerController extends Controller
{

    public function actionDb()
    {
        DatabaseConfig::create($this);
    }

    public function actionAdmin()
    {
        UserAdmin::add($this);
    }

}

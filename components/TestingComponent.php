<?php

namespace app\components;

use app\helpers\TestHelper;
use app\models\AR\Testing;
use yii\base\Component;
use yii\helpers\VarDumper;

/**
 * Class TestingComponent
 * @package app\components
 */

class TestingComponent extends Component
{

    public $test = null;

    public function __construct(array $config = [])
    {
        $this->test = Testing::findOne(['token' => \Yii::$app->session->get('token') ]);

        if (!$this->test) {
            $this->test = $this->getTestByUserId();
        }

        parent::__construct($config);
    }

    public function getTest()
    {
        return $this->test;
    }


    public function getTestByUserId()
    {
        return Testing::findOne(['user_id' => \Yii::$app->user->id]);
    }

    /**
     * Открытость
     * @return bool
     */
    public function isOpen()
    {
        $result = false;

        if (!$this->getTest()) {
            return false;
        }

        if ($this->test->time == null) {
            return true;
        }

        if (strtotime(date("Y-m-d H:i:s")) - $this->test->time < 1800) {
            $result = true;
        } else {
            return $result;
        }

        $cnt = 0;
        $questions = TestHelper::parseQuestions($this->test->data);

        foreach ($questions as $item) {
            if ($item['isAnswered'] == 'true') {
                $cnt++;
            }
        }

        if ($cnt !== count($questions)) {
            $result = true;
        }

        return $result;
    }


    /**
     * Стартовано
     * @return bool
     */
    public function isStarted()
    {
        if (!$this->getTest()) {
            return false;
        }

        if ($this->test->time == null) {
            return false;
        }

        return true;
    }




}
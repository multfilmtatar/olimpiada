<?php

use yii\db\Migration;

/**
 * Class m190929_104724_change_testing
 */
class m190929_104724_change_testing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%testing}}', 'endtime', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190929_104724_change_testing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190929_104724_change_testing cannot be reverted.\n";

        return false;
    }
    */
}

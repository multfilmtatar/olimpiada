<?php

use yii\db\Migration;

/**
 * Class m191001_051218_set_status_curator_3
 */
class m191001_051218_set_status_curator_3 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = \webvimark\modules\UserManagement\models\User::find()->all();

        foreach ($users as $user) {
            \webvimark\modules\UserManagement\models\User::assignRole($user->id, 'curator');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191001_051218_set_status_curator_3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191001_051218_set_status_curator_3 cannot be reverted.\n";

        return false;
    }
    */
}
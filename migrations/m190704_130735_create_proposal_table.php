<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%proposal}}`.
 */
class m190704_130735_create_proposal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%proposal}}', [
            'id' => $this->primaryKey(),
            'status_id' => $this->smallInteger(1)->defaultValue(1)->comment(''),
            'type' => $this->smallInteger(1)->defaultValue(0)->comment('Тип заявки (отборочный или финал)'),
            'curator_id' => $this->integer()->comment('Куратор'),
            'username' => $this->string()->notNull()->comment('Имя пользователя'),
            'birthday' => $this->date()->notNull()->comment('Дата рождения'),
            'document_number' => $this->string()->notNull()->comment('№ свидетельства о рождении'),
            'school' => $this->string()->notNull()->comment('Образовательная организация'),
            'class' => $this->string()->notNull()->comment('Класс'),
            'address' => $this->string()->comment('Домашний адрес'),
            'phone' => $this->string()->notNull()->comment('Номер телефона'),
            'user_id' => $this->integer()->comment('Пользователь')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%proposal}}');
    }
}

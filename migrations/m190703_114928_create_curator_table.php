<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%curator}}`.
 */
class m190703_114928_create_curator_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%curator}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'username' => $this->string()->notNull()->comment('ФИО'),
            'district_id' => $this->integer()->notNull()->comment('Район')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%curator}}');
    }
}

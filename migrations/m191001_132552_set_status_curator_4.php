<?php

use yii\db\Migration;

/**
 * Class m191001_132552_set_status_curator_4
 */
class m191001_132552_set_status_curator_4 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = \webvimark\modules\UserManagement\models\User::find()->all();

        foreach ($users as $user) {
            \webvimark\modules\UserManagement\models\User::assignRole($user->id, 'curator');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191001_132552_set_status_curator_4 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191001_132552_set_status_curator_4 cannot be reverted.\n";

        return false;
    }
    */
}
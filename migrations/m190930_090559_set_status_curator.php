<?php

use yii\db\Migration;

/**
 * Class m190930_090559_set_status_curator
 */
class m190930_090559_set_status_curator extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = \webvimark\modules\UserManagement\models\User::find()->all();

        foreach ($users as $user) {
            \webvimark\modules\UserManagement\models\User::assignRole($user->id, 'curator');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190930_090559_set_status_curator cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_090559_set_status_curator cannot be reverted.\n";

        return false;
    }
    */
}
<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%result}}`.
 */
class m190716_070712_create_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%result}}', [
            'id' => $this->primaryKey(),
            'proposal_id' => $this->integer()->comment('Заявка'),
            'district_id' => $this->integer()->comment('Район'),
            'sum' => $this->integer()->comment('Итог')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%result}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%testing}}`.
 */
class m190711_055213_create_testing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->comment('Вопрос'),
            'img' => $this->string()->notNull()->comment('Изображение'),
        ]);

        $this->createTable('{{%answer}}', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer()->comment('Вопрос'),
            'title' => $this->string()->notNull()->comment('Ответ'),
            'img' => $this->string()->notNull()->comment('Изображение'),
            'isRight' => $this->boolean()->defaultValue(false)->comment('Правильный')
        ]);

        $this->createTable('{{%testing}}', [
            'id' => $this->primaryKey(),
            'proposal_id' => $this->integer()->comment('Заявка'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'data' => $this->text()->comment('Данные теста'),
            'sum' => $this->integer()->defaultValue(0)->comment('Результат'),
            'token' => $this->string(35)->comment('Токен доступа'),
            'time' => $this->integer()->comment('Дата и время начала тестирования')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%testing}}');
    }
}

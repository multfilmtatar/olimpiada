<?php

namespace app\controllers;

use app\helpers\AccessHelper;
use app\models\AR\ProposalAR;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class FinishController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {
                            return AccessHelper::isAllowedByFinish();
                        }
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    throw new NotFoundHttpException('Page not found');
                }
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Yii::$app->testing->getTest();

        if ($model && !$model->endtime) {
            $model->endtime = strtotime(date("Y-m-d H:i:s"));
            $model->save(false);
        }

        $minutes = ($model->endtime - $model->time) / 60 % 60;
        $seconds = abs($model->endtime - $model->time) % 60;

        if ($minutes < 10) {
            $minutes = '0'.$minutes;
        }
        if ($seconds < 10) {
            $seconds = '0'.$seconds;
        }

        return $this->render('index', [
            'model' => $model,
            'tm' => $minutes . ':' . $seconds
        ]);
    }

}


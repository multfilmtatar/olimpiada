<?php

namespace app\controllers;

use app\helpers\TestHelper;
use app\models\AR\ProposalAR;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ApiController extends Controller
{
    protected $model;

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Page not found');
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (($this->model = Yii::$app->testing->getTest()) == null) {
            return [
                'code' => 200,
                'redirect' => Url::base(true) . '/finish'
            ];
        }

        if (!Yii::$app->testing->isOpen()) {
            return [
                'code' => 200,
                'redirect' => Url::base(true) . '/finish'
            ];
        }

        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        $prop = ProposalAR::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->one();

        $questions = TestHelper::parseQuestions($this->model->data);

        return [
            'code' => 200,
            'data' => $questions,
            'start_time' => $this->model->time,
            'username' => $prop ? $prop->username : ''
        ];
    }

    public function actionAnswer()
    {
        $request = Yii::$app->request;

        $question_id = intval($request->post('question_id'));
        $answer_id = intval($request->post('answer_id'));

        return TestHelper::checkQuestion($question_id, $answer_id);
    }

    public function actionFinish()
    {
        return [
            'code' => 200,
            'redirect' => Url::base(true) . '/finish'
        ];
    }

}
<?php

namespace app\controllers;

use app\helpers\AccessHelper;
use app\helpers\TestHelper;
use app\models\AR\Testing;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TestController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'start'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {return true;
                            return AccessHelper::isAllowed();
                        }
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    if (\Yii::$app->curator->curator) {
                        throw new NotFoundHttpException('Page not found');
                    }

                    return $this->redirect('/finish');
                }
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->testing->isStarted()) {
            return $this->redirect('/test/start');
        }

        return $this->render('index');
    }

    public function actionStart()
    {
        //* для закрытия тестирования раскоментить строчку ниже
        // return $this->redirect('/');
        
        $model = $this->findModel();

        if ($model && !$model->time) {
            $model->time = strtotime(date("Y-m-d H:i:s"));
            $model->save(false);
        }


//        if (!Yii::$app->testing->isOpen()) {
//            return $this->redirect('/finish');
//        }

        $questions = TestHelper::parseQuestions($model->data);

        if (!$questions) {
            return $this->redirect('/finish');
        }

        return $this->render('start');
    }

    /**
     * @return bool | Testing
     * @throws NotFoundHttpException
     */
    protected function findModel()
    {
        if (($model = Yii::$app->testing->getTest()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
<?php

namespace app\controllers;

use app\helpers\TestHelper;
use app\models\AR\ProposalAR;
use app\models\LoginForm;
use app\models\ProposalForm;
use app\models\RegistrationForm;
use webvimark\modules\UserManagement\components\UserIdentity;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\httpclient\Client;

class RegisterController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'login', 'register'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return $this->redirect('/test');
                }
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new ProposalForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $model->setCookies();

            $loginModel = new LoginForm();
            $loginModel->login = $model->document_number;
            $loginModel->password = $model->document_number;

            $user = $loginModel->autoRegister($loginModel);

            $u = new \Yii::$app->user->identityClass;

            /* @var $u UserIdentity */
            Yii::$app->user->login($u->findByUsername($user->username), Yii::$app->user->cookieLifetime);

            $prop = ProposalAR::findOne(\Yii::$app->session->get('proposalId'));

            if (!$prop->user_id) {
                $prop->user_id = Yii::$app->user->id;
                $prop->save(false);
            }

            TestHelper::create();

            return $this->redirect('/test');
        }

        return $this->render('_proposal-form', [
            'model' => $model
        ]);
    }

}
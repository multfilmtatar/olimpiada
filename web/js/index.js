(function() {
    $(document).ready(function() {
        console.log(platform.name);
        console.log(+platform.version);

        if (platform.name == "IE" && +platform.version < 10) {
            $(".js-start-test").click(function(e) {
                e.preventDefault();
            });
            $(".js-start-test").after(
                '<div class="alert alert-danger" role="alert" style="margin-top: 15px;">Данный браузер не поддерживается! Пожалуйста, воспользуйтесь одним из рекомендуемых ниже браузеров.</div>'
            );
        }
    });
})();

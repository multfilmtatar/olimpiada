"use strict";

function App() {
    return (
        <div className="App">
            <Test />
        </div>
    );
}

class Test extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            componentState: 1
        };

        this.startDate = null;
        this.userName = null;
        this.data = [];
    }

    pressStart = () => {
        // console.log("press start");
        this.setState({
            componentState: 1
        });

        // тут надо fetch
        // fetch("/api/index", {
        //     // method: "post",
        //     // headers: {
        //     //     Accept: "application/json",
        //     //     "Content-Type": "application/json"
        //     // },
        //     // body: { field: "" }
        // })
        //     .then(response => {
        //         console.log(response);
        //         return response.json();
        //     })
        //     .then(result => console.log(result))
        //     .catch(error => console.log("Ошибка:", error));

        this.getData();

        // console.log("data", data);

        // setTimeout(() => {
        //     // todo проверить пройден ли тест
        //     this.setState({
        //         componentState: 2
        //     });
        // }, 1000);
    };

    getData = () => {
        let ths = this;
        $.ajax({
            data: { field: "" },
            dataType: "json",
            success: function(data) {
                // console.log(data);

                ths.startDate = data.start_time;
                ths.userName = data.username;

                for (const key in data.data) {
                    if (data.data.hasOwnProperty(key)) {
                        const item = data.data[key];
                        ths.data.push(item);
                    }
                }

                ths.setState({
                    componentState: 2
                });
            },
            type: "post",
            url: "/api/index"
        }).fail(function(data) {
            // console.log(data);
            ths.getData();
        });
    };

    endTime = () => {
        window.location.href = "/finish";
        // window.location.reload(true);

        // console.log("time is end");
        // this.setState({
        //     componentState: 3
        // });
    };

    endTest = () => {
        window.location.href = "/finish";
        // window.location.reload(true);

        // this.setState({
        //     componentState: 4
        // });
    };

    getContent() {
        switch (this.state.componentState) {
            case 0: // стартовое состояние
                return (
                    <div className="">
                        <button onClick={this.pressStart}>Начать тест</button>
                    </div>
                );
            case 1: // ожидание ответа сервера
                return <div>Подождите...</div>;
            case 2: // показ теста
                return (
                    <div>
                        <TestContent data={this.data} endTest={this.endTest} userName={this.userName} />
                        <Time endTime={this.endTime} startDate={this.startDate} />
                    </div>
                );
            case 3: // Вышло время
                return <div>Время вышло</div>;
            case 4: // Тест пройден
                return <div>Тест пройден!!!</div>;
            default:
                break;
        }
    }

    componentWillMount() {
        this.getData();
    }

    render() {
        return <div className="test">{this.getContent()}</div>;
    }
}

class TestContent extends React.Component {
    constructor(props) {
        super(props);

        this.skippedQuestion = [];
        this.data = props.data;
        this.sendError = [];

        this.state = {
            curQuestion: this.checkQuestionIndex(0),
            selectedOption: ""
        };
    }

    handleFormSubmit = e => {
        e.preventDefault();

        let { curQuestion, selectedOption } = this.state;

        if (selectedOption) {
            this.data[curQuestion].isAnswered = true;

            // console.log(this.data);

            this.sendAnswer(this.data[curQuestion].id, selectedOption);

            let nextQuestion = this.checkQuestionIndex(curQuestion + 1, curQuestion + 1);

            if (this.data[nextQuestion] && !this.data[nextQuestion].isAnswered) {
                this.setState({
                    curQuestion: nextQuestion,
                    selectedOption: ""
                });
            } else if (this.skippedQuestion.length === 0) {
                if (this.sendError.length === 0) {
                    this.props.endTest();
                } else {
                    // console.log(this.sendError);
                    this.setState({
                        curQuestion: -1
                    });
                    this.repeatSend();
                }
            } else if (this.skippedQuestion.length > 0) {
                nextQuestion = this.skippedQuestion.shift();

                this.setState({
                    curQuestion: nextQuestion,
                    selectedOption: ""
                });
            }
        }
    };

    sendAnswer = (questionId, answerId) => {
        // console.log(questionId, answerId);
        let ths = this;
        $.ajax({
            data: { question_id: +questionId, answer_id: +answerId },
            dataType: "json",
            success: function(data) {
                // console.log(data);
            },
            type: "post",
            url: "/api/answer"
        }).fail(function(data) {
            // console.log("fail send answer", data);
            ths.sendError.push([questionId, answerId]);
        });
    };

    handleOptionChange = e => {
        this.setState({
            selectedOption: e.target.value
        });
    };

    checkQuestionIndex = (current, start = 0) => {
        if (this.data[current] && this.data[current].isAnswered) {
            for (let i = start; i < this.data.length; i++) {
                if (this.data[i] && !this.data[i].isAnswered) {
                    current = i;
                    break;
                }
            }
        } else if (!this.data[current]) {
            current = this.checkQuestionIndex(0);
        }

        return current;
    };

    renderContent = () => {
        let { curQuestion } = this.state;

        if (this.skippedQuestion.length > 0) {
            let pos = this.skippedQuestion.indexOf(curQuestion);

            if (pos > -1) {
                this.skippedQuestion.splice(pos, 1);
            }
        }

        // console.log(this.data[curQuestion]);
        if (this.data[curQuestion]) {
            let data = this.data[curQuestion].answers,
                answers = [];

            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    const element = data[key];

                    answers.push(
                        <div key={element.id} className="test-form__group">
                            <label className="test-form__label">
                                <input
                                    type="radio"
                                    value={element.id}
                                    checked={this.state.selectedOption === "" + element.id}
                                    onChange={this.handleOptionChange}
                                    className="test-form__radio"
                                />
                                <span className="test-form__item">{element.title}</span>
                            </label>
                        </div>
                    );
                }
            }

            return (
                <form onSubmit={this.handleFormSubmit} className="test__form test-form">
                    <div className="test-form__header clearfix">
                        <div className="test-form__qnumb">
                            <span className="test-form__qnumb-num">{curQuestion + 1}</span>
                            <div className="clearfix"></div>
                            <span className="test-form__qnumb-title">Вопрос</span>
                        </div>
                        <div className="test-form__title">{this.data[curQuestion].title}</div>
                    </div>
                    {answers}
                </form>
            );
        }

        return false;
    };

    renderThumb = () => {
        let { curQuestion } = this.state;

        return this.data.map((item, i) => {
            let dopClass = "";

            if (i === curQuestion) dopClass += " active";
            if (item.isAnswered) dopClass += " done";

            return (
                <span key={item.title} className={"test__thumb" + dopClass}>
                    {i + 1}
                </span>
            );
        });
    };

    pressSkip = () => {
        if (this.skippedQuestion.indexOf(this.state.curQuestion)) {
            this.skippedQuestion.push(this.state.curQuestion);
        }

        let cur = this.checkQuestionIndex(this.state.curQuestion + 1, this.state.curQuestion + 1);

        if (!this.data[cur].isAnswered) {
            if (this.state.curQuestion !== cur) {
                this.setState(state => {
                    return { curQuestion: cur, selectedOption: "" };
                });
            }
        }
    };

    repeatSend = () => {
        let ths = this;

        if (this.sendError.length > 0) {
            let question = this.sendError.pop();
            $.ajax({
                data: { question_id: +question[0], answer_id: +question[1] },
                dataType: "json",
                success: function(data) {
                    ths.repeatSend();
                },
                type: "post",
                url: "/api/answer"
            }).fail(function(data) {
                ths.sendError.push([question[0], question[1]]);

                ths.repeatSend();
            });
        } else {
            this.props.endTest();
        }
    };

    render() {
        if (this.state.curQuestion > -1) {
            return (
                <div>
                    <div className="row">
                        <div className="col-xs-6">
                            <h1 className="text-left">Тестирование</h1>
                        </div>
                        <div className="col-xs-6">
                            <div className="test__user-name">{this.props.userName}</div>
                        </div>
                    </div>
                    {this.renderContent()}
                    <div className="text-center">
                        <button className="btn btn-default default-button test__button" type="submit" onClick={this.handleFormSubmit}>
                            Выбрать
                        </button>

                        <button className="btn btn-default default-button default-button_invert test__button" type="submit" onClick={this.pressSkip}>
                            Пропустить
                        </button>
                    </div>
                    <div className="test__thumb-wrap">{this.renderThumb()}</div>
                </div>
            );
        } else {
            return <div>Подождите...</div>;
        }
    }
}

class Time extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: props.startDate,
            endDate: props.startDate + 1800,
            now: Math.floor(Date.now() / 1000),
            minutes: null,
            seconds: null
        };
    }

    setSeconds = () => {
        let { startDate, now, endDate } = this.state;

        if (endDate - now <= 1800) {
            let secDiff = endDate - now;
            let minDiff = secDiff / 60;

            // console.log(Math.floor(minDiff), secDiff % 60);

            this.setState(
                {
                    seconds: secDiff
                },
                () => this.setTimer()
            );
        }

        return "00:00";
    };

    setTimer = () => {
        this.timer = setInterval(() => {
            if (this.state.seconds > 0) {
                this.setState(state => {
                    return {
                        minutes: Math.floor(state.seconds / 60),
                        seconds: state.seconds - 1
                    };
                });
            } else {
                clearInterval(this.timer);

                this.props.endTime();
            }
        }, 1000);
    };

    renderMinutes = () => {
        let min = this.state.minutes;

        if (min < 10) {
            return "0" + min;
        }

        return min;
    };

    renderSeconds = () => {
        let sec = this.state.seconds % 60;

        if (sec < 10) {
            return "0" + sec;
        }

        return sec;
    };

    componentDidMount() {
        this.setSeconds();
    }

    render() {
        if (this.state.seconds !== null && this.state.minutes !== null) {
            return (
                <div className="test__timer">
                    {this.renderMinutes()}:{this.renderSeconds()}
                </div>
            );
        }
        return <div className="test__timer">--:--</div>;
    }
}

ReactDOM.render(<App />, document.getElementById("root"));

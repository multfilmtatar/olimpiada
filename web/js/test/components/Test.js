import React, { Component } from "react";
import TestContent from "./TestContent";
import Time from "./Time";

const data = [
    {
        title: "Title 1",
        answers: ["111111111111111111111", "111111111111111111111", "111111111111111111111"],
        checked: "o"
    },
    {
        title: "Title 2",
        answers: ["222222222222222222", "222222222222222222", "222222222222222222"],
        checked: "ks"
    },
    {
        title: "Title 3",
        answers: ["333333333333333333", "333333333333333333", "333333333333333333"],
        checked: "ks"
    }
];

export default class Test extends Component {
    constructor(props) {
        super(props);

        this.state = {
            componentState: 0
        };
    }

    pressStart = () => {
        console.log("press start");
        this.setState({
            componentState: 1
        });

        // тут надо fetch
        setTimeout(() => {
            // todo проверить пройден ли тест
            this.setState({
                componentState: 2
            });
        }, 1000);
    };

    endTime = () => {
        console.log("time is end");
        this.setState({
            componentState: 3
        });
    };

    endTest = () => {
        this.setState({
            componentState: 4
        });
    };

    getContent() {
        switch (this.state.componentState) {
            case 0: // стартовое состояние
                return (
                    <div className="">
                        <button onClick={this.pressStart}>Начать тест</button>
                    </div>
                );
            case 1: // ожидание ответа сервера
                return <div>Waiting...</div>;
            case 2: // показ теста
                return (
                    <div>
                        <TestContent data={data} endTest={this.endTest} />
                        <Time endTime={this.endTime} />
                    </div>
                );
            case 3: // Вышло время
                return <div>Время вышло</div>;
            case 4: // Тест пройден
                return <div>Тест пройден!!!</div>;
            default:
                break;
        }
    }

    render() {
        return <div>{this.getContent()}</div>;
    }
}

import React, { Component } from "react";
import Timer from "react-compound-timer";

export default class Time extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date() - 1800 * 1000,
            now: new Date()
        };
    }

    setTimer = () => {
        let { startDate, now } = this.state;

        console.log(now - startDate);
        if (now - startDate <= 1800000) {
            return (
                <Timer
                    initialTime={now - startDate}
                    direction="backward"
                    checkpoints={[{ time: 0, callback: () => this.props.endTime() }]}
                    formatValue={value => `${value < 10 ? `0${value}` : value}`}
                >
                    {() => (
                        <React.Fragment>
                            <Timer.Minutes />:
                            <Timer.Seconds />
                        </React.Fragment>
                    )}
                </Timer>
            );
        }

        return "00:00";
    };

    render() {
        return <div>{this.setTimer()}</div>;
    }
}

import React, { Component } from "react";

export default class TestContent extends Component {
    constructor(props) {
        super(props);

        this.skippedQuestion = [];
        this.data = props.data;

        this.state = {
            curQuestion: this.checkQuestionIndex(0),
            selectedOption: ""
        };
    }

    handleFormSubmit = e => {
        e.preventDefault();

        // console.log(e.target);

        let { curQuestion, selectedOption } = this.state;

        this.data[curQuestion].checked = selectedOption;

        console.log(this.data);

        let nextQuestion = this.checkQuestionIndex(curQuestion + 1, curQuestion + 1);

        if (this.data[nextQuestion] && !this.data[nextQuestion].checked) {
            // console.log("1");
            this.setState({
                curQuestion: nextQuestion,
                selectedOption: ""
            });
        } else if (this.skippedQuestion.length === 0) {
            // console.log("2");
            this.props.endTest();
        } else if (this.skippedQuestion.length > 0) {
            // console.log("3");
            nextQuestion = this.skippedQuestion.shift();

            this.setState({
                curQuestion: nextQuestion,
                selectedOption: ""
            });
        }

        // if (curQuestion < this.data.length - 1) {
        //     console.log("1");
        //     this.setState(state => {
        //         return { curQuestion: state.curQuestion + 1, selectedOption: "" };
        //     });
        // } else if (this.skippedQuestion.length === 0) {
        //     console.log("2");
        //     this.props.endTest();
        // }

        // тут отправить результат на сервак
    };

    handleOptionChange = e => {
        // console.log(e.target);
        this.setState({
            selectedOption: e.target.value
        });
    };

    checkQuestionIndex = (current, start = 0) => {
        if (this.data[current] && this.data[current].checked) {
            for (let i = start; i < this.data.length; i++) {
                if (!this.data[i].checked) {
                    current = i;
                    break;
                }
            }
        }

        return current;
    };

    renderContent = () => {
        let { curQuestion } = this.state;
        // console.log("4");

        // console.log(this.skippedQuestion);

        if (this.skippedQuestion.length > 0) {
            let pos = this.skippedQuestion.indexOf(curQuestion);

            if (pos > -1) {
                // console.log("slice");
                // console.log(curQuestion, pos);
                this.skippedQuestion.splice(pos, 1);
                // console.log(this.skippedQuestion);
            }
        }

        if (this.data[curQuestion]) {
            // console.log("5");
            let answers = this.data[curQuestion].answers.map((item, i) => (
                <div key={i} className="radio">
                    <label>
                        <input type="radio" value={"option" + i} checked={this.state.selectedOption === "option" + i} onChange={this.handleOptionChange} />
                        {item}
                    </label>
                </div>
            ));
            return (
                <form onSubmit={this.handleFormSubmit}>
                    <div>{this.data[curQuestion].title}</div>
                    {answers}
                </form>
            );
        } else if (this.skippedQuestion.length > 0) {
            // console.log("6");
            // let cur = this.skippedQuestion.shift();
            // this.setState({
            //     curQuestion: cur,
            //     selectedOption: ""
            // });
        }

        // this.props.endTest();
        return false;
        // return <div>Тест пройден</div>;
    };

    renderThumb = () => {
        let { curQuestion } = this.state;
        // console.log(this.data);
        return this.data.map((item, i) => {
            let dopClass = "";

            // console.log(this.data[curQuestion]);
            if (i === curQuestion) dopClass += " active";
            if (item.checked) dopClass += " done";

            return (
                <span key={item.title} className={"thumb" + dopClass}>
                    {i + 1}
                </span>
            );
        });
    };

    pressSkip = () => {
        if (this.skippedQuestion.indexOf(this.state.curQuestion)) {
            this.skippedQuestion.push(this.state.curQuestion);
        }

        let cur = this.checkQuestionIndex(this.state.curQuestion + 1, this.state.curQuestion + 1);

        if (!this.data[cur]) cur = this.checkQuestionIndex(0);
        // console.log(cur, "cur");
        this.setState(state => {
            return { curQuestion: cur, selectedOption: "" };
        });
    };

    // shouldComponentUpdate(nextProps, nextState) {
    //     if (nextState.curQuestion >= this.data.length) return false;

    //     return true;
    // }

    render() {
        return (
            <div>
                {this.renderContent()}
                <div>
                    {this.state.selectedOption && (
                        <button className="btn btn-default" type="submit" onClick={this.handleFormSubmit}>
                            Выбрать
                        </button>
                    )}
                    <button className="btn btn-default" type="submit" onClick={this.pressSkip}>
                        Пропустить
                    </button>
                </div>
                {this.renderThumb()}
            </div>
        );
    }
}

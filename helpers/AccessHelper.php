<?php

namespace app\helpers;

use Yii;

class AccessHelper
{
    public static function isAllowed()
    {
        if (\Yii::$app->curator->curator) {
            return false;
        }

        if (!Yii::$app->testing->isOpen()) {
            return false;
        }

        return true;
    }

    public static function isAllowedByFinish()
    {
        if (\Yii::$app->curator->curator) {
            return false;
        }

        return true;
    }
}
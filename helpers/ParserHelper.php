<?php

namespace app\helpers;


class ParserHelper
{
    public static function start($questions)
    {
        $result = [];

        foreach ($questions as $question) {
            $result[$question->id] = [
                'id' => $question->id,
                'response_id' => null
            ];
        }

        return $result;
    }

}
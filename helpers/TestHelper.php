<?php

namespace app\helpers;

use app\models\AR\Answer;
use app\models\AR\ProposalAR;
use app\models\AR\Question;
use Yii;
use app\models\AR\Testing;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

class TestHelper
{
    public static function create()
    {
        $proposal_id = Yii::$app->session->get('proposalId');

        if (ProposalAR::findOne(['user_id' => Yii::$app->user->id, 'id' => $proposal_id]) == null) {
            return false;
        }

        if (($test = Testing::findOne(['user_id' => Yii::$app->user->id])) == null) {
            $test = new Testing();
            $test->proposal_id = $proposal_id;
            $test->user_id = Yii::$app->user->id;
            $test->data = self::generateQuestions();
            $test->sum = 0;
            $test->token = Yii::$app->security->generateRandomString(30);
            $test->time = null;
            $test->save();
        }

        //Yii::$app->session->remove('token');
        Yii::$app->session->set('token', $test->token);

        return true;
    }

    public static function parseQuestions($items)
    {
        $items = Json::decode($items, true);
        $keys = ArrayHelper::getColumn($items, 'id');
        $result = [];

        $questions = Question::find()
            ->where(['IN', 'id', $keys])
            ->with(['answers'])
            ->all();

        foreach ($questions as $question) {
            if (!$items[$question->id]['response_id']) {
                $result[$question->id] = [
                    'id' => $question->id,
                    'title' => $question->title,
                    'img' => Url::base(true) . '/' . $question->img,
                    'isAnswered' => $items[$question->id]['response_id'] ? true : false
                ];

                foreach ($question->answers as $answer) {
                    $result[$question->id]['answers'][$answer->id] = [
                        'id' => $answer->id,
                        'title' => $answer->title,
                        'img' => Url::base(true) . '/' . $answer->img
                    ];
                }
            }
        }

        return $result;
    }

    public static function checkQuestion($question_id, $answer_id)
    {
        if ($question_id && $answer_id) {
            $model = Yii::$app->testing->getTest();
            $questions = Json::decode($model->data);

            if (!ArrayHelper::keyExists($question_id, $questions)) {
                return [
                    'code' => 404,
                    'message' => 'Обнаружена ошибка. Повторите попытку'
                ];
            }

            $answer = Answer::find()
                ->where(['question_id' => $question_id, 'id' => $answer_id])
                ->one();


            if ($answer) {
                if ($questions[$question_id]['response_id'] == null) {
                    $questions[$question_id]['response_id'] = $answer_id;

                    $model->data = Json::encode($questions);
                    $model->sum = $answer->isRight ? $model->sum + 1 : $model->sum;
                    $model->save(false);
                }

                return [
                    'code' => 200,
                    'message' => 'success'
                ];
            }

            return [
                'code' => 200,
                'message' => 'Не засчитано'
            ];

        }

        return [
            'code' => 404,
            'message' => 'Обнаружена ошибка. Повторите попытку'
        ];
    }

    protected static function generateQuestions()
    {
        $questions = Question::find()
            ->orderBy(new Expression('rand()'))
            ->limit(30)
            ->all();

        $result = ParserHelper::start($questions);

        return Json::encode($result);
    }
}